var mongoose = require('mongoose');
var userSheme = new mongoose.Schema({
    name: String,
    surname: String,
    phone: String,
    email: String
});

mongoose.model('User', userSheme, 'users');