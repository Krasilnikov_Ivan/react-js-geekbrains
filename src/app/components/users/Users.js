import React from 'react';
import UserList from './UserList';
import UserStore from '../../stores/usersStore';
import {addUser, fetchUsers} from '../../actions/usersActions';


export default class Users extends React.Component {
    constructor() {
        super(...arguments);

        this.state = {
            users: []
        };

        this.newUser = this.newUser.bind(this);
        this.onUsersChange = this.onUsersChange.bind(this);
    }

    newUser()
    {
        let name = 'Новый пользователь';
        let email = 'Email пользователя';
        let phone = 'Телефон пользователя';
        let website = 'майт пользователя';

        addUser({name, email, phone, website});
    }

    onUsersChange(users)
    {
        this.setState({
            users
        });
    }

    componentDidMount()
    {
        fetchUsers();
    }

    componentWillMount(){
        UserStore.on('change', this.onUsersChange);
    }

    componentWillUnmount()
    {
        UserStore.removeListener('change', this.onUsersChange);

    }

    render() {
        return(
            <div>
                <button type="button" className="btn btn-primary" onClick={this.newUser}>Добавить пользователя</button>
                <UserList users={this.state.users} />
            </div>
        );
    }
}