import {
    ADD_USER,
    FETCH_USERS_START,
    FETCH_USERS_END
} from '../constants/usersConstants';

import dispatcher from '../dispatcher';
import axios from 'axios';

import { EventEmitter } from 'events';

class UsersStore extends EventEmitter {
    constructor()
    {
        super(...arguments);

        this.users = [];

        this.fetchUsersStart = this.fetchUsersStart.bind(this);
        this.fetchUsersEnd = this.fetchUsersEnd.bind(this);
        this.change = this.change.bind(this);
        this.getUsers = this.getUsers.bind(this);
        this.handleActions = this.handleActions.bind(this);
    }

    fetchUsersStart(){
        axios
            //.get('https://jsonplaceholder.typicode.com/users')
            .get('http://localhost:8083/api/users/')
            .then((response) => {
                let { data } = response;

                dispatcher.dispatch(
                    {
                        type: FETCH_USERS_END,
                        payload: data
                    }
                );
            });
    }

    fetchUsersEnd(users)
    {
        this.users = users;
        this.change();
    }

    change()
    {
        this.emit('change', this.users);
    }

    getUsers()
    {
        return this.users;
    }

    addUser(user)
    {
        this.users.push(user);
        this.change();
    }

    handleActions(action)
    {
        switch (action.type)
        {
            case ADD_USER:
            {
                this.addUser(action.payload);
                break;
            }
            case FETCH_USERS_START:
            {
                this.fetchUsersStart();
                break;
            }
            case FETCH_USERS_END:
            {
                this.fetchUsersEnd(action.payload);
                break;
            }
        }
    }
}

const usersStore = new UsersStore;

dispatcher.register(usersStore.handleActions);

export default usersStore;