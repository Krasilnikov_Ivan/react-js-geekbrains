import dispatcher from '../dispatcher';
import {ADD_USER, FETCH_USERS_START} from '../constants/usersConstants';

export function addUser({name, email, phone, website}) {
    const user = {name, email, phone, website};

    dispatcher.dispatch({
        type: ADD_USER,
        payload: user
    });
}

export function fetchUsers() {
    dispatcher.dispatch({
        type: FETCH_USERS_START
    });
}